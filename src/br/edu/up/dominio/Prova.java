package br.edu.up.dominio;

public class Prova {

	private TipoDeProva tipo;
	private double nota;
	
	public TipoDeProva getTipo() {
		return tipo;
	}

	public void setTipo(TipoDeProva tipo) {
		this.tipo = tipo;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}
}
